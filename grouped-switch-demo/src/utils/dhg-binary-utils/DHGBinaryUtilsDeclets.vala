namespace DHGBinaryUtils {

// Declet masks.
const uint64[] DECLET_MASKS = {
	//(( 1 << 30 ) - 1) - (( 1 << 20 ) - 1) and alike
	1023, 1047552, 1072693248, 1098437885952, 1124800395214848, 1151795604700004352
	};

/**
 * Declet related class. All methods are static.
 */
public class Declets {
/**
 * Gets an declet at the position. Position must be between 0 and 2.
 */
public static uint get_uint(uint num, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 3)
	{
	return ( num & DECLET_MASKS[position + 3] ) >> (( 2 - position ) * 10 );
	}
/**
 * Gets an declet at the position. Position must be between 0 and 2.
 * Explicit 32 bit lenght.
 */
public static uint32 get_uint32(uint32 num, uint position)
requires (position >= 0 && position < 3)
	{
	return ( num & DECLET_MASKS[position + 3] ) >> (( 2 - position ) * 10 );
	}
/**
 * Gets an declet at the position. Position must be between 0 and 6.
 * Explicit 64 bit lenght.
 */
public static uint64 get_uint64(uint64 num, uint position)
requires (position >= 0 && position < 7)
	{
	return ( num & DECLET_MASKS[position] ) >> (( 2 - position ) * 10 );
	}

/**
 * Sets an declet specified by position to a value specified by include.
 * Position must be between 0 and 2. Returns the new value of num.
 */
public static uint set_uint(uint num, uint include, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 3)
	{
	return num ^ (( num ^ include << (( 2 - position ) * 10 ) ) & DECLET_MASKS[position + 3] );
	}

/**
 * Sets an declet specified by position to a value specified by include. Does
 * setting in-place. Position must be between 0 and 2.
 */
public static void set_uint_out(out uint num, uint include, uint position)
requires (position >= 0 && position < 3)
	{
	num = num ^ (( num ^ include << (( 2 - position ) * 10 ) ) & DECLET_MASKS[position + 3] );
	}

/**
 * Sets an declet specified by position to a value specified by include.
 * Position must be between 0 and 2. Returns the new value of num.
 * Explicit 32 bit lenght.
 */
public static uint32 set_uint32(uint32 num, uint include, uint position)
requires (position >= 0 && position < 3)
	{
	return num ^ (( num ^ include << (( 2 - position ) * 10 ) ) & DECLET_MASKS[position + 3] );
	}

/**
 * Sets an declet specified by position to a value specified by include. Does
 * setting in-place. Position must be between 0 and 2.
 * Explicit 32 bit lenght.
 */
public static void set_uint32_out(out uint32 num, uint include, uint position)
requires (position >= 0 && position < 3)
	{
	num = num ^ (( num ^ include << (( 2 - position ) * 10 ) ) & DECLET_MASKS[position + 3] );
	}

/**
 * Sets an declet specified by position to a value specified by include.
 * Position must be between 0 and 6. Returns the new value of num.
 * Explicit 64 bit lenght.
 */
public static uint64 set_uint64(uint64 num, uint include, uint position)
requires (position >= 0 && position < 7)
	{
	return (uint64)( num ^ (( num ^ include << (( 2 - position ) * 10 ) ) & DECLET_MASKS[position] ));
	}
/**
 * Sets an declet specified by position to a value specified by include. Does
 * setting in-place. Position must be between 0 and 6.
 * Explicit 64 bit lenght.
 */
public static void set_uint64_out(out uint64 num, uint include, uint position)
requires (position >= 0 && position < 7)
	{
	num = num ^ (( num ^ include << (( 2 - position ) * 10 ) ) & DECLET_MASKS[position] );
	}

/**
 * Compares declets specified by position in two source numbers. Returns true
 * if they match or false otherwise. Position can be between 0 and 2.
 */
public static bool uint_cmp(uint num1, uint num2, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 3)
	{
	return ((( num1 ^ num2 ) & DECLET_MASKS[position + 3] )==0 );
	}
/**
 * Compares declets specified by position in two source numbers. Returns true
 * if they match or false otherwise. Position can be between 0 and 2.
 * Explicit 32 bit lenght.
 */
public static bool uint32_cmp(uint32 num1, uint32 num2, uint position)
requires (position >= 0 && position < 3)
	{
	return ((( num1 ^ num2 ) & DECLET_MASKS[position + 3] )==0 );
	}
/**
 * Compares declets specified by position in two source numbers. Returns true
 * if they match or false otherwise. Position can be between 0 and 6.
 * Explicit 64 bit lenght.
 */
public static bool uint64_cmp(uint64 num1, uint64 num2, uint position)
requires (position >= 0 && position < 7)
	{
	return ((( num1 ^ num2 ) & DECLET_MASKS[position] )==0 );
	}

/**
 * Utility method returning an int being a difference between two declets
 * specified by position. Position can be between 0 and 2.
 */
public static int uint_diff(uint num1, uint num2, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 3)
	{
	return (int)( get_uint(num1, position) - get_uint(num2, position));
	}
/**
 * Utility method returning an int32 being a difference between two declets
 * specified by position. Position can be between 0 and 2.
 * Explicit 32 bit lenght.
 */
public static int32 uint32_diff(uint32 num1, uint32 num2, uint position)
requires (position >= 0 && position < 3)
	{
	return (int32)( get_uint32(num1, position) - get_uint32(num2, position));
	}
/**
 * Utility method returning an int32 being a difference between two declets
 * specified by position. Position can be between 0 and 6.
 * Explicit 64 bit lenght.
 */
public static int32 uint64_diff(uint64 num1, uint64 num2, uint position)
requires (position >= 0 && position < 7)
	{
	return (int32)( get_uint64(num1, position) - get_uint64(num2, position));
	}

/**
 * Utility function returning an int being a sum of two declets
 * specified by position. Position can be between 0 and 2.
 */
public static int uint_sum(uint num1, uint num2, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 3)
	{
	return (int)( get_uint(num1, position) + get_uint(num2, position));
	}
/**
 * Utility function returning an int32 being a sum of two declets
 * specified by position. Position can be between 0 and 2.
 * Explicit 32 bit lenght.
 */
public static int32 uint32_sum(uint32 num1, uint32 num2, uint position)
requires (position >= 0 && position < 3)
	{
	return (int32)( get_uint32(num1, position) + get_uint32(num2, position));
	}
/**
 * Utility function returning an int32 being a sum of two declets
 * specified by position. Position can be between 0 and 6.
 * Explicit 64 bit lenght.
 */
public static int32 uint64_sum(uint64 num1, uint64 num2, uint position)
requires (position >= 0 && position < 7)
	{
	return (int32)( get_uint64(num1, position) + get_uint64(num2, position));
	}
}

}
