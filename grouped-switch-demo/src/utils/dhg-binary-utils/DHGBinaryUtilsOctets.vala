namespace DHGBinaryUtils {

// Octet masks.
const uint64[] OCTET_MASKS = {
	//(( 1 << 32 ) - 1) - (( 1 << 24 ) - 1) and alike
	255, 65280, 16711680, 4278190080, 1095216660480, 280375465082880, 71776119061217280, 18374686479671623680
	};

/**
 * Octet related class. All methods are static.
 */
public class Octets {
/**
 * Gets an octet at the position. Position must be between 0 and 3.
 */
public static uint get_uint(uint num, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 4)
	{
	return ( num & OCTET_MASKS[position + 4] ) >> (( 3 - position ) * 8 );
	}
/**
 * Gets an octet at the position. Position must be between 0 and 3.
 * Explicit 32 bit lenght.
 */
public static uint32 get_uint32(uint32 num, uint position)
requires (position >= 0 && position < 4)
	{
	return ( num & OCTET_MASKS[position + 4] ) >> (( 3 - position ) * 8 );
	}
/**
 * Gets an octet at the position. Position must be between 0 and 7.
 * Explicit 64 bit lenght.
 */
public static uint64 get_uint64(uint64 num, uint position)
requires (position >= 0 && position < 8)
	{
	return ( num & OCTET_MASKS[position] ) >> (( 3 - position ) * 8 );
	}

/**
 * Sets an octet specified by position to a value specified by include.
 * Position must be between 0 and 3. Returns the new value of num.
 */
public static uint set_uint(uint num, uint include, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 4)
	{
	return num ^ (( num ^ include << (( 3 - position ) * 8 ) ) & OCTET_MASKS[position + 4] );
	}

/**
 * Sets an octet specified by position to a value specified by include. Does
 * setting in-place. Position must be between 0 and 3.
 */
public static void set_uint_out(out uint num, uint include, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 4)
	{
	num = num ^ (( num ^ include << (( 3 - position ) * 8 ) ) & OCTET_MASKS[position + 4] );
	}

/**
 * Sets an octet specified by position to a value specified by include.
 * Position must be between 0 and 3. Returns the new value of num.
 * Explicit 32 bit lenght.
 */
public static uint32 set_uint32(uint32 num, uint include, uint position)
requires (position >= 0 && position < 4)
	{
	return num ^ (( num ^ include << (( 3 - position ) * 8 ) ) & OCTET_MASKS[position + 4] );
	}
/**
 * Sets an octet specified by position to a value specified by include. Does
 * setting in-place. Position must be between 0 and 3.
 * Explicit 32 bit lenght.
 */
public static void set_uint32_out(out uint32 num, uint include, uint position)
requires (position >= 0 && position < 4)
	{
	num = num ^ (( num ^ include << (( 3 - position ) * 8 ) ) & OCTET_MASKS[position + 4] );
	}

/**
 * Sets an octet specified by position to a value specified by include.
 * Position must be between 0 and 7. Returns the new value of num.
 * Explicit 64 bit lenght.
 */
public static uint64 set_uint64(uint64 num, uint include, uint position)
requires (position >= 0 && position < 8)
	{
	return (uint64)( num ^ (( num ^ include << (( 3 - position ) * 8 ) ) & OCTET_MASKS[position] ));
	}

/**
 * Sets an octet specified by position to a value specified by include. Does
 * setting in-place. Position must be between 0 and 7.
 * Explicit 64 bit lenght.
 */
public static void set_uint64_out(out uint64 num, uint include, uint position)
requires (position >= 0 && position < 8)
	{
	num = num ^ (( num ^ include << (( 3 - position ) * 8 ) ) & OCTET_MASKS[position] );
	}

/**
 * Compares octets specified by position in two source numbers. Returns true
 * if they match or false otherwise. Position can be between 0 and 3.
 */
public static bool uint_cmp(uint num1, uint num2, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 4)
	{
	return ((( num1 ^ num2 ) & OCTET_MASKS[position + 4] )==0 );
	}
/**
 * Compares octets specified by position in two source numbers. Returns true
 * if they match or false otherwise. Position can be between 0 and 3.
 * Explicit 32 bit lenght.
 */
public static bool uint32_cmp(uint32 num1, uint32 num2, uint position)
requires (position >= 0 && position < 4)
	{
	return ((( num1 ^ num2 ) & OCTET_MASKS[position + 4] )==0 );
	}
/**
 * Compares octets specified by position in two source numbers. Returns true
 * if they match or false otherwise. Position can be between 0 and 7.
 * Explicit 64 bit lenght.
 */
public static bool uint64_cmp(uint64 num1, uint64 num2, uint position)
requires (position >= 0 && position < 8)
	{
	return ((( num1 ^ num2 ) & OCTET_MASKS[position] )==0 );
	}

/**
 * Utility method returning an int being a difference between two octets
 * specified by position. Position can be between 0 and 3.
 */
public static int uint_diff(uint num1, uint num2, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 4)
	{
	return (int)( get_uint(num1, position) - get_uint(num2, position));
	}
/**
 * Utility method returning an int32 being a difference between two octets
 * specified by position. Position can be between 0 and 3.
 * Explicit 32 bit lenght.
 */
public static int32 uint32_diff(uint32 num1, uint32 num2, uint position)
requires (position >= 0 && position < 4)
	{
	return (int32)( get_uint32(num1, position) - get_uint32(num2, position));
	}
/**
 * Utility method returning an int32 being a difference between two octets
 * specified by position. Position can be between 0 and 7.
 * Explicit 64 bit lenght.
 */
public static int32 uint64_diff(uint64 num1, uint64 num2, uint position)
requires (position >= 0 && position < 8)
	{
	return (int32)( get_uint64(num1, position) - get_uint64(num2, position));
	}

/**
 * Utility function returning an int being a sum of two octets
 * specified by position. Position can be between 0 and 3.
 */
public static int uint_sum(uint num1, uint num2, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 4)
	{
	return (int)( get_uint(num1, position) + get_uint(num2, position));
	}
/**
 * Utility function returning an int32 being a sum of two octets
 * specified by position. Position can be between 0 and 3.
 * Explicit 32 bit lenght.
 */
public static int32 uint32_sum(uint32 num1, uint32 num2, uint position)
requires (position >= 0 && position < 4)
	{
	return (int32)( get_uint32(num1, position) + get_uint32(num2, position));
	}
/**
 * Utility function returning an int32 being a sum of two octets
 * specified by position. Position can be between 0 and 7.
 * Explicit 64 bit lenght.
 */
public static int32 uint64_sum(uint64 num1, uint64 num2, uint position)
requires (position >= 0 && position < 8)
	{
	return (int32)( get_uint64(num1, position) + get_uint64(num2, position));
	}
}

}
