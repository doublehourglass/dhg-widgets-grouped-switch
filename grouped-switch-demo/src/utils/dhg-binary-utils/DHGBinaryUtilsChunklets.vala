namespace DHGBinaryUtils {

/**
 * chunklet related class. All methods are static.
 */
public class Chunklets {
/**
 * Gets an chunklet of size at a position. Position must be between 0 and 31.
 * Size between 1 and 32.
 */
public static uint get_uint(uint num, uint position, uint size = 8)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	return ( num & ((( 1 << size ) - 1 ) << ( position * size )) ) >> ( size * position );
	}
/**
 * Gets an chunklet of size at a position. Position must be between 0 and 31.
 * Size between 1 and 32. Explicit 32 bit lenght.
 */
public static uint32 get_uint32(uint32 num, uint position, uint size = 8)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	return ( num & ((( 1 << size ) - 1 ) << ( position * size )) ) >> ( size * position );
	}
/**
 * Gets an chunklet of size at a position. Position must be between 0 and 63.
 * Size between 1 and 64. Explicit 64 bit lenght.
 */
public static uint64 get_uint64(uint64 num, uint position, uint size = 8)
requires (position >= 0 && position < 64)
requires (size > 0 && size <= 64)
requires (size * ( position + 1 ) <= 64)
	{
	return ( num & ((( 1 << size ) - 1 ) << ( position * size )) ) >> ( size * position );
	}

/**
 * Sets a chunklet of size located at a position to a value specified
 * by include. Does setting in-place. Position must be between 0 and 31,
 * size between 1 and 32.
 */
public static uint set_uint(uint num, uint include, uint position, uint size = 8)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	return num ^ (( num ^ include << ( size * position ) ) & ((( 1 << size ) - 1 ) << ( position * size )) );
	}
/**
 * Sets a chunklet of size located at a position to a value specified
 * by include. Does setting in-place. Position must be between 0 and 31,
 * size between 1 and 32. Explicit 32 bit lenght.
 */
public static void set_uint_out(out uint num, uint include, uint position, uint size = 8)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	num = num ^ (( num ^ include << ( size * position ) ) & ((( 1 << size ) - 1 ) << ( position * size )) );
	}

/**
 * Sets a chunklet of size located at a position to a value specified
 * by include. Does setting in-place. Position must be between 0 and 31,
 * size between 1 and 32.
 */
public static uint32 set_uint32(uint32 num, uint include, uint position, uint size = 8)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	return num ^ (( num ^ include << ( size * position ) ) & ((( 1 << size ) - 1 ) << ( position * size )) );
	}
/**
 * Sets a chunklet of size located at a position to a value specified
 * by include. Does setting in-place. Position must be between 0 and 31,
 * size between 1 and 32. Explicit 32 bit lenght.
 */
public static void set_uint32_out(out uint32 num, uint include, uint position, uint size = 8)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	num = num ^ (( num ^ include << ( size * position ) ) & ((( 1 << size ) - 1 ) << ( position * size )) );
	}

/**
 * Sets an chunklet specified by position to a value specified by include.
 * Position must be between 0 and 63. Returns the new value of num.
 * Explicit 64 bit lenght.
 */
public static uint64 set_uint64(uint64 num, uint include, uint position, uint size = 8)
requires (position >= 0 && position < 64)
requires (size > 0 && size <= 64)
requires (size * ( position + 1 ) <= 64)
	{
	return (uint64)( num ^ (( num ^ include << ( size * position ) ) & ((( 1 << size ) - 1 ) << ( position * size )) ));
	}
/**
 * Sets a chunklet of size located at a position to a value specified
 * by include. Does setting in-place. Position must be between 0 and 63,
 * size between 1 and 64. Explicit 64 bit lenght.
 */
public static void set_uint64_out(out uint64 num, uint include, uint position, uint size = 8)
requires (position >= 0 && position < 64)
requires (size > 0 && size <= 64)
requires (size * ( position + 1 ) <= 64)
	{
	num = num ^ (( num ^ include << ( size * position ) ) & ((( 1 << size ) - 1 ) << ( position * size )) );
	}

/**
 * Compares chunklets specified by size and position in two source numbers.
 * Returns true if they match or false otherwise.
 * Position can be between 0 and 31, size between 1 and 32.
 */
public static bool uint_cmp(uint num1, uint num2, uint position, uint size = 8)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	return ((( num1 ^ num2 ) & ((( 1 << size ) - 1 ) << ( position * size )) )==0 );
	}
/**
 * Compares chunklets specified by size and position in two source numbers.
 * Returns true if they match or false otherwise.
 * Position can be between 0 and 31, size between 1 and 32.
 * Explicit 32 bit lenght.
 */
public static bool uint32_cmp(uint32 num1, uint32 num2, uint position, uint size = 8)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	return ((( num1 ^ num2 ) & ((( 1 << size ) - 1 ) << ( position * size )) )==0 );
	}

/**
 * Compares chunklets specified by size and position in two source numbers.
 * Returns true if they match or false otherwise.
 * Position can be between 0 and 63, size between 1 and 64.
 * Explicit 32 bit lenght.
 */
public static bool uint64_cmp(uint64 num1, uint64 num2, uint position, uint size = 8)
requires (position >= 0 && position < 64)
requires (size > 0 && size <= 64)
requires (size * ( position + 1 ) <= 64)
	{
	return ((( num1 ^ num2 ) & ((( 1 << size ) - 1 ) << ( position * size )) )==0 );
	}

/**
 * Utility method returning an int being a difference between two chunklets
 * of size specified by position. Position can be between 0 and 31,
 * size between 1 and 32.
 */
public static int uint_diff(uint num1, uint num2, uint position, uint size = 8)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	return (int)( get_uint(num1, position, size) - get_uint(num2, position, size));
	}
/**
 * Utility method returning an int32 being a difference between two chunklets
 * of size specified by position. Position can be between 0 and 31,
 * size between 1 and 32. Explicit 32 bit lenght.
 */
public static int32 uint32_diff(uint32 num1, uint32 num2, uint position, uint size = 8)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	return (int32)( get_uint32(num1, position, size) - get_uint32(num2, position, size));
	}
/**
 * Utility method returning an int64 being a difference between two chunklets
 * of size specified by position. Position can be between 0 and 63,
 * size between 1 and 64. Explicit 64 bit lenght.
 */
public static int32 uint64_diff(uint64 num1, uint64 num2, uint position, uint size = 8)
requires (position >= 0 && position < 64)
requires (size > 0 && size <= 64)
requires (size * ( position + 1 ) <= 64)
	{
	return (int32)( get_uint64(num1, position, size) - get_uint64(num2, position, size));
	}
/**
 * Utility method returning an int being a sum of two chunklets of size
 * specified by position. Position can be between 0 and 31,
 * size between 1 and 32.
 */
public static int uint_sum(uint num1, uint num2, uint position, uint size = 8)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	return (int)( get_uint(num1, position, size) + get_uint(num2, position, size));
	}
/**
 * Utility method returning an int32 being a sum of two chunklets of size
 * specified by position. Position can be between 0 and 31,
 * size between 1 and 32. Explicit 32 bit lenght.
 */
public static int32 uint32_sum(uint32 num1, uint32 num2, uint position, uint size = 8)
requires (position >= 0 && position < 32)
requires (size > 0 && size <= 32)
requires (size * ( position + 1 ) <= 32)
	{
	return (int32)( get_uint32(num1, position, size) + get_uint32(num2, position, size));
	}
/**
 * Utility method returning an int64 being a sum of two chunklets of size
 * specified by position. Position can be between 0 and 63,
 * size between 1 and 64. Explicit 64 bit lenght.
 */
public static int32 uint64_sum(uint64 num1, uint64 num2, uint position, uint size = 8)
requires (position >= 0 && position < 64)
requires (size > 0 && size <= 64)
requires (size * ( position + 1 ) <= 64)
	{
	return (int32)( get_uint64(num1, position, size) + get_uint64(num2, position, size));
	}
}

}
