namespace DHGBinaryUtils {

// Hextet masks.
const uint64[] HEXTET_MASKS = {
	//(( 1 << 32 ) - 1) - (( 1 << 16 ) - 1) and alike
	65535, 4294901760, 281470681743360, 18446462598732840960
	};
/**
 * Hextet related class. All methods are static.
 */
public class Hextets {
/**
 * Gets a hextet at the position. Position must be between 0 and 1.
 */
public static uint get_uint(uint num, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 2)
	{
	return ( num & HEXTET_MASKS[position + 2] ) >> (( 1 - position ) * 16 );
	}
/**
 * Gets a hextet at the position. Position must be between 0 and 1.
 * Explicit 32 bit lenght.
 */
public static uint32 get_uint32(uint32 num, uint position)
requires (position >= 0 && position < 2)
	{
	return ( num & HEXTET_MASKS[position + 2] ) >> (( 1 - position ) * 16 );
	}
/**
 * Gets a hextet at the position. Position must be between 0 and 3.
 * Explicit 64 bit lenght.
 */
public static uint64 get_uint64(uint64 num, uint position)
requires (position >= 0 && position < 4)
	{
	return ( num & HEXTET_MASKS[position] ) >> (( 1 - position ) * 16 );
	}

/**
 * Sets a hextet specified by position to a value specified by include.
 * Position must be between 0 and 1. Returns the new value of num.
 */
public static uint set_uint(uint num, uint include, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 2)
	{
	return num ^ (( num ^ include << (( 1 - position ) * 16 ) ) & HEXTET_MASKS[position + 2] );
	}

/**
 * Sets a hextet specified by position to a value specified by include. Does
 * setting in-place. Position must be between 0 and 1.
 */
public static void set_uint_out(out uint num, uint include, uint position)
requires (position >= 0 && position < 2)
	{
	num = num ^ (( num ^ include << (( 1 - position ) * 16 ) ) & HEXTET_MASKS[position + 2] );
	}

/**
 * Sets a hextet specified by position to a value specified by include.
 * Position must be between 0 and 1. Returns the new value of num.
 * Explicit 32 bit lenght.
 */
public static uint32 set_uint32(uint32 num, uint include, uint position)
requires (position >= 0 && position < 2)
	{
	return num ^ (( num ^ include << (( 1 - position ) * 16 ) ) & HEXTET_MASKS[position + 2] );
	}

/**
 * Sets a hextet specified by position to a value specified by include. Does
 * setting in-place. Position must be between 0 and 1.
 * Explicit 32 bit lenght.
 */
public static void set_uint32_out(out uint32 num, uint include, uint position)
requires (position >= 0 && position < 2)
	{
	num = num ^ (( num ^ include << (( 1 - position ) * 16 ) ) & HEXTET_MASKS[position + 2] );
	}

/**
 * Sets a hextet specified by position to a value specified by include.
 * Position must be between 0 and 3. Returns the new value of num.
 * Explicit 64 bit lenght.
 */
public static uint64 set_uint64(uint64 num, uint include, uint position)
requires (position >= 0 && position < 4)
	{
	return (uint64)( num ^ (( num ^ include << (( 1 - position ) * 16 ) ) & HEXTET_MASKS[position] ));
	}
/**
 * Sets a hextet specified by position to a value specified by include. Does
 * setting in-place. Position must be between 0 and 3.
 * Explicit 64 bit lenght.
 */
public static void set_uint64_out(out uint64 num, uint include, uint position)
requires (position >= 0 && position < 4)
	{
	num = num ^ (( num ^ include << (( 1 - position ) * 16 ) ) & HEXTET_MASKS[position] );
	}

/**
 * Compares hextets specified by position in two source numbers. Returns true
 * if they match or false otherwise. Position can be between 0 and 1.
 */
public static bool uint_cmp(uint num1, uint num2, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 2)
	{
	return ((( num1 ^ num2 ) & HEXTET_MASKS[position + 2] )==0 );
	}
/**
 * Compares hextets specified by position in two source numbers. Returns true
 * if they match or false otherwise. Position can be between 0 and 1.
 * Explicit 32 bit lenght.
 */
public static bool uint32_cmp(uint32 num1, uint32 num2, uint position)
requires (position >= 0 && position < 2)
	{
	return ((( num1 ^ num2 ) & HEXTET_MASKS[position + 2] )==0 );
	}
/**
 * Compares hextets specified by position in two source numbers. Returns true
 * if they match or false otherwise. Position can be between 0 and 3.
 * Explicit 64 bit lenght.
 */
public static bool uint64_cmp(uint64 num1, uint64 num2, uint position)
requires (position >= 0 && position < 4)
	{
	return ((( num1 ^ num2 ) & HEXTET_MASKS[position] )==0 );
	}

/**
 * Utility method returning an int being a difference between two hextets
 * specified by position. Position can be between 0 and 1.
 */
public static int uint_diff(uint num1, uint num2, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 2)
	{
	return (int)( get_uint(num1, position) - get_uint(num2, position));
	}
/**
 * Utility method returning an int32 being a difference between two hextets
 * specified by position. Position can be between 0 and 1.
 * Explicit 32 bit lenght.
 */
public static int32 uint32_diff(uint32 num1, uint32 num2, uint position)
requires (position >= 0 && position < 2)
	{
	return (int32)( get_uint32(num1, position) - get_uint32(num2, position));
	}
/**
 * Utility method returning an int32 being a difference between two hextets
 * specified by position. Position can be between 0 and 3.
 * Explicit 64 bit lenght.
 */
public static int32 uint64_diff(uint64 num1, uint64 num2, uint position)
requires (position >= 0 && position < 4)
	{
	return (int32)( get_uint64(num1, position) - get_uint64(num2, position));
	}

/**
 * Utility function returning an int being a sum of two hextets
 * specified by position. Position can be between 0 and 1.
 */
public static int uint_sum(uint num1, uint num2, uint position)
requires (uint.MAX >= 4294967295)
requires (position >= 0 && position < 2)
	{
	return (int)( get_uint(num1, position) + get_uint(num2, position));
	}
/**
 * Utility function returning an int32 being a sum of two hextets
 * specified by position. Position can be between 0 and 1.
 * Explicit 32 bit lenght.
 */
public static int32 uint32_sum(uint32 num1, uint32 num2, uint position)
requires (position >= 0 && position < 2)
	{
	return (int32)( get_uint32(num1, position) + get_uint32(num2, position));
	}
/**
 * Utility function returning an int32 being a sum of two hextets
 * specified by position. Position can be between 0 and 3.
 * Explicit 64 bit lenght.
 */
public static int32 uint64_sum(uint64 num1, uint64 num2, uint position)
requires (position >= 0 && position < 4)
	{
	return (int32)( get_uint64(num1, position) + get_uint64(num2, position));
	}
}

}
