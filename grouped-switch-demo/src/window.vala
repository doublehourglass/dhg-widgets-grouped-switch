/* window.vala
 *
 * Copyright 2019 doublehourglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gtk;
using DHGWidgets;

namespace Groupedswitch {
[GtkTemplate (ui = "/me/dhg/groupedswitch/window.ui")]
public class Window : Gtk.ApplicationWindow {
[GtkChild]
Gtk.Box main;

Gtk.Button[] buttons = {
	new Gtk.Button.with_label ("L1"),
	new Gtk.Button.with_label ("L2"),
	new Gtk.Button.with_label ("L3")
	};

Gtk.ButtonBox box = new Gtk.ButtonBox (Gtk.Orientation.HORIZONTAL);

public Window (Gtk.Application app) {
	Object (application: app);
	}
construct {
	box.set_layout (Gtk.ButtonBoxStyle.START);
	foreach ( unowned Gtk.Button button in buttons ) {
		box.add (button);
		}
	main.add(new Label("AAAAAAAAA"));
	string[] list = {"ABC", "BCD", "EFG", "HIJ"};

	GLib.ListStore examp;
	var lm = new GLib.ListStore(typeof( GSModelItem ));
	for ( uint i = 0; i<list.length; i++ ) {
		int rand = Random.int_range(0, 80);
		GLib.ListStore coll = new GLib.ListStore(typeof( Label ));
		for ( uint j = 0; j<rand; j++ ) {
			coll.append(new Label("ZZZ"));
			}
		if ( i==1 ) examp = coll;
		lm.append(new GSModelItem(list[i], coll, 20));
		}
	buttons[1].clicked.connect(( ) => {
				examp.append(new Label("YYY"));
				// message("CLIIICK %u", examp.get_n_items());
				});
	buttons[0].clicked.connect(( ) => {
				if ( examp.get_n_items()>0 ) examp.remove(0);
				// message("REM %u", examp.get_n_items());
				});

	var w = new GroupedSwitch(lm);
	main.add(w);
	buttons[2].clicked.connect(( ) => {
				w.select_next();
				});
	main.add(box);
	show_all();
	}

}
}
