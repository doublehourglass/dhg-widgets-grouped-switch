using Gtk;
using Gdk;
using Gee;
using Samples;
namespace DHGWidgets {

public class GSGButton : Button {
public uint position {get; set;}
public GSGButton(uint position) {
	Object (position: position);
	}
construct {
	set_hexpand(false);
	int pwidth = 0;
	int nwidth = 0;

	}
static construct {
	set_css_name("gsgbutton");
	}
}
public class GSGLabel : Label {
public GSGLabel(string? text) {
	Object(label: text);
	}
static construct {
	set_css_name("gsglabel");
	}
}


[GtkTemplate (ui = "/me/dhg/groupedswitch/GroupedSwitchGroup.ui")]
public class GroupedSwitchGroup : EventBox {
[GtkChild]
ButtonBox buttons_box;
[GtkChild]
Grid main_grid;
// [GtkChild]
// Label title_label;

public string uuid {get; set;}
public Object item {get; set;}
public bool selected = false;
public uint position {get; set;}
public bool display_button_labels = false;
Gee.ArrayList<GSGButton> buttons {get; set;}

bool init_lock = true;

public struct Selection {
	uint button;
	bool group_selected;
	}

Selection _selection = Selection() {
	button = 0, group_selected = false
	};
public uint? selection {
	get{
		if ( !_selection.group_selected ) {
			return null;
			} else {
			return _selection.button;
			}
		}
	set{
		if ( value==null ) {
			_selection = Selection() {
				button = 0, group_selected = false
				};
			} else {
			_selection = Selection() {
				button = value, group_selected = true
				};
			}
		}
	}

uint _pages_number = 0;
public uint pages_number {
	get{
		return _pages_number;
		}
	set{
		_pages_number = value;
		if ( !init_lock ) update_pages(value);
		}
	}

uint _items_per_block;
public uint items_per_block {
	get {return _items_per_block;}
	set {
		_items_per_block = value;
		pages_number = _items_number / _items_per_block;
		}
	default = 1;
	}
uint _items_number;
public uint items_number {
	get {
		return _items_number;
		}
	set {
		_items_number = value;
		pages_number = ( _items_number==0 )? 0 : _items_number / _items_per_block + 1;
		}
	default = 0;
	}

public void add_cb(GSGButton button) {
	button.clicked.connect(button_clicked);
	}

public void select(uint num) {
	var child_buttons = buttons_box.get_children();
	var btn = child_buttons.nth_data(num);
	btn.set_state_flags(StateFlags.CHECKED, true);
	}
public void deselect(uint num) {
	var child_buttons = buttons_box.get_children();
	var btn = child_buttons.nth_data(num);
	btn.unset_state_flags(StateFlags.CHECKED);
	}


public void button_clicked(Button button){
	message("CLICKED %u", ((GSGButton)button ).position);
	selection = ((GSGButton)button ).position;
	button.set_state_flags(StateFlags.CHECKED, true);
	state_updated(position, selection);
	}

public bool group_clicked(Widget w, EventButton event){
	var child_buttons = buttons_box.get_children();
	var btn = child_buttons.nth_data(0);
	if ( event.button == 1 ) {
		selection = 0;
		message("CLICKED group %u", position);
		btn.set_state_flags(StateFlags.CHECKED, true);
		state_updated(position, selection);

		}
	// button.set_state_flags(StateFlags.CHECKED, true);
	// message("CLICKED group");
	return false;
	}

public void update_pages(uint curr_pages_num) {
	var child_buttons = buttons_box.get_children();
	var curr_buttons_number = child_buttons.length();
	if ( curr_buttons_number==curr_pages_num ) return;
	if ( curr_buttons_number<curr_pages_num ) {
		for ( uint i = curr_buttons_number; i<curr_pages_num; i++ ) {
			// var btn = new Button();
			var btn = new GSGButton(i);
			buttons_box.add((Button)btn);
			// buttons.add(btn);
			add_cb(btn);
			}
		buttons_box.show_all();
		} else {
		for ( uint i = curr_buttons_number; i>curr_pages_num; i-- ) {
			Widget? btn = child_buttons.nth_data(i - 1);
			if ( btn!=null ) btn.destroy();
			}
		}
	message("CHANGE! %u %u", curr_pages_num, curr_buttons_number);
	buttons_updated(position, curr_pages_num);
	}

public signal void buttons_updated(uint position, uint buttons_number);
public signal void state_updated(uint position, uint? selection);

static construct {
	set_css_name("groupedswitchgroup");
	}

//Masked constructor
GroupedSwitchGroup() {
	Object (
		uuid: uuid,
		item: item
		);

	}

public void model_updated(uint position, uint removed, uint added) {
	// update of items number must be performed with initial caching as otherwise update_pages method
	// will fire twice.
	uint temp_items_number = items_number + added;
	temp_items_number -= removed;
	items_number = temp_items_number;
	}

public GroupedSwitchGroup.from_model(string uuid, GSModelItem item, uint position) {
	Object (
		items_per_block: item.items_per_block,
		items_number: item.items.get_n_items(),
		position: position
		);
	init_lock = false;
	main_grid.attach(new GSGLabel(item.name), 0, 0);
	update_pages(pages_number);
	message("NAME %s", item.name);
	message("PAGES %u", pages_number);
	item.items.items_changed.connect(model_updated);
	}
public GroupedSwitchGroup.from_number(string uuid, GSItem item, uint position) {
	message("NAME %s", ((GSItem)item ).name);

	}

construct {
	button_press_event.connect(group_clicked);
	// title_label.set_label("SMTH");
	}

}
}
