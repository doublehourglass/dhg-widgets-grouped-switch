using Gtk;
using Gdk;
using Gee;
using Samples;
using DHGBinaryUtils;
namespace DHGWidgets {

public class GSItem : Object {
public string name {get; set;}
public uint items {get; set;}
public GSItem(string name, uint count) {
	Object (name: name, items: count);
	}
}
public class GSModelItem : GSItem {
public new GLib.ListStore items {get; set;}
public uint items_per_block {get; set;}
public GSModelItem(string name, GLib.ListStore list_store, uint items_per_block) {
	Object (name: name, items_per_block: items_per_block, items: list_store);
	}
}

public class GroupedSwitch : Gtk.Grid {

public struct SelectionState {
	uint group;
	uint page;
	public SelectionState(uint g, uint p) {
		group = g;
		page = p;
		}
	}

public SelectionState selection_state;

const string DEFAULT_CSS_RESOURCE = "/me/dhg/groupedswitch/GroupedSwitch.css";

static CssProvider _provider = new CssProvider();

public GLib.ListStore model {get; set;}
public Stack managed_stack {get; set;}
public Type items_type {get; construct;}


public Gee.HashMap<string, string> items_index = new Gee.HashMap<string, string> ();
public Gee.HashMap<string, string> groups_index = new Gee.HashMap<string, string> ();
public Gee.HashMap<string, GroupedSwitchGroup> groups = new Gee.HashMap<string, GroupedSwitchGroup> ();
public Gee.ArrayList<GroupedSwitchGroup> groups_list = new Gee.ArrayList<GroupedSwitchGroup> ();

public GroupedSwitch(GLib.ListStore list_model, uint initial_group = 0, uint initial_page = 0) {
	Object (items_type: list_model.item_type);
	bind_model(list_model);
	selection_state = SelectionState(initial_group, initial_page);
	}

static construct {
	set_css_name("groupedswitch");
	_provider.load_from_resource(DEFAULT_CSS_RESOURCE);
	StyleContext.add_provider_for_screen (Screen.get_default(), _provider, STYLE_PROVIDER_PRIORITY_APPLICATION);
	// kbd_bindings = new DndOrderListBoxBindings(BindingSet.by_class((ObjectClass) ( typeof (
	// DndOrderListBox )).class_ref()));
	}
construct {}

public void state_updated(uint position, uint? selection) {
	var sg = groups_list[(int)position];
	int sel = (int)( selection ?? -1 );
	if ( position==selection_state.group && selection_state.page==sel ) return;
	GroupedSwitchGroup? desel_group = groups_list[(int)( selection_state.group )];
	if ( desel_group==null ) return;

	desel_group.deselect(selection_state.page);

	message("SEL STATE %u %u", selection_state.group, selection_state.page);
	message("STATE UPDATED %u %u %d", sg.position, sg.pages_number, sel);
	selection_state = SelectionState(sg.position, sel);
	}

public void select_num() {}


// r = a ^ ((a ^ b) & mask);
// unsigned int a;    // value to merge in non-masked bits
// unsigned int b;    // value to merge in masked bits
// unsigned int mask; // 1 where bits from b should be selected; 0 where from a.
// unsigned int r;    // result of (a & ~mask) | (b & mask) goes here

public enum EXIT_CODE {
	A = 104,
	B = 203;
	}


public void select_next() {
	Octets.uint_cmp(0, 0, 0);
	uint8 smth = 132;
	uint8 full = 255;

	message("BBB: %u", uint.MAX );
	uint8 v1 = 11;
	uint8 v2 = 13;
	uint8 v3 = 25;

	uint8 v2alt = 72;
	uint accu = 0;
	// accu = ( v1 << 24 ) + ( v2 << 16 ) + ( v3 << 8 );
	accu = Octets.set_uint(accu, v1, 0);
	accu = Octets.set_uint(accu, v2, 1);
	accu = Octets.set_uint(accu, v3, 2);
	message("ACCU:%s", accu.to_string());
	message("V1 = %u", Octets.get_uint(accu, 0));
	message("V2 = %u", Octets.get_uint(accu, 1));
	message("V3 = %u", Octets.get_uint(accu, 2));
	message("V4 = %u", Octets.get_uint(accu, 3));

	uint accu1 = accu;
	accu1 = Octets.set_uint(accu1, v2alt, 3);
	accu1 = Octets.set_uint(accu1, v2alt, 2);
	message("ACCU1:%s", accu1.to_string());

	message("V1 = %u", Octets.get_uint(accu1, 0));
	message("V2 = %u", Octets.get_uint(accu1, 1));
	message("V3 = %u", Octets.get_uint(accu1, 2));
	message("V4 = %u", Octets.get_uint(accu1, 3));

	message(Octets.uint_cmp(accu, accu1, 0).to_string());
	message(Octets.uint_cmp(accu, accu1, 1).to_string());
	message(Octets.uint_cmp(accu, accu1, 2).to_string());
	message(Octets.uint_cmp(accu, accu1, 3).to_string());

	message(Octets.uint_diff(accu, accu1, 2).to_string());


	// uint16 rest = (uint16)accu;
	// message("REST:%s", rest.to_string());
	//
	// uint16 msa = (uint16)( accu >> 16 );
	// message("MSA:%s", msa.to_string());
	GroupedSwitchGroup? current_selection = groups_list[(int)( selection_state.group )];
	if ( selection_state.page + 2<=current_selection.pages_number && selection_state.page>0 ) {
		// message("aaaa");
		} else {
		// message("bbbb %u %u", selection_state.page, current_selection.pages_number - 1);
		}
	}

public void buttons_updated(uint position, uint new_buttons_number) {
	var sg = groups_list.@get((int)position);
	message("BUTTONS UPDATED %u %u", sg.position, new_buttons_number);
	}

public void bind_model(GLib.ListStore model) {
	uint counter = 0;
	GSItem? item;
	while ( ( item = (GSItem)( model.get_item(counter++)))!=null ) {
		string uuid = Uuid.string_random();
		GroupedSwitchGroup? g = null;
		if ( items_type==typeof( GSModelItem )) {
			g = new GroupedSwitchGroup.from_model(uuid, (GSModelItem)item, counter - 1);
			groups_list.add(g);
			g.buttons_updated.connect(buttons_updated);
			g.state_updated.connect(state_updated);
			groups.set(((GSModelItem)item ).name, g);
			}
		// 	else
		// if ( items_type==typeof( GSItem )) {
		// 	g = new GroupedSwitchGroup.from_number(uuid, (GSItem)item, counter - 1);
		// 	groups_list.add(g);
		// 	g.buttons_updated.connect(buttons_updated);
		// 	groups.set(((GSItem)item ).name, g);
		// 	} else {}
		add(g);
		}
	}
}
}
